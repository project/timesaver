<?php

/**
 * @file
 * Timesaver configuration items file.
 *
 * These are simple configuration items used by Timesaver and/or the Timesaver report generator.
 * It is not advisable to change anything in this file other than the $_timesaver_LANG_* items
 */
global $_TIMESAVER_CONF, $_TIMESAVER_LANG_REPORT_COLUMNS, $_TIMESAVER_LANG_REPORT_FREE_FORM_COLUMNS;
$_TIMESAVER_CONF=array();

$_TIMESAVER_CONF['table_class_array']=array(
    'table_timesheet_entry',
    'view_timesaver_data',
    'table_timesaver_locked_timesheets'
);

$_TIMESAVER_CONF['day_offsets']=array(
    '0' => t('Sunday'),
    '1' => t('Monday'),
    '2' => t('Tuesday'),
    '3' => t('Wednesday'),
    '4' => t('Thursday'),
    '5' => t('Friday'),
    '6' => t('Saturday')
);

$_TIMESAVER_CONF['report_columns']=array( 'SPACER',
    'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
    'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
    'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
    'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ'
);

$_TIMESAVER_LANG_REPORT_COLUMNS=array(
    'task_number'           =>  t('Task #'),
    'timesaver_activity_id' =>  t('Activity'),
    'task_id'               =>  t('Task'),
    'project_number'        =>  t('Project #'),
    'regular_time'          =>  t('Reg. Time'),
    'time_1_5'              =>  t('Time @150%'),
    'time_2_0'              =>  t('Time @200%'),
    'evening_hours'         =>  t('Evening Hours'),
    'stat_time'             =>  t('Statutory'),
    'vacation_time_used'    =>  t('Vacation Time'),
    'floater'               =>  t('Floater'),
    'sick_time'             =>  t('Paid Sick Time'),
    'bereavement'           =>  t('Bereavement'),
    'jury_duty'             =>  t('Jury Duty'),
    'total_reg_hours'       =>  t('Total HRS. Paid'),
    'unpaid_hrs'            =>  t('Unpaid Hrs.'),
    'other'                 =>  t('Other'),
    'comment'               =>  t('Comment')
);

$_TIMESAVER_LANG_REPORT_FREE_FORM_COLUMNS=array(
    'thedate'               =>  t('Date'),
    'fullname'              =>  t('Full Name'),
    'timesaver_activity_id' =>  t('Activity'),
    'project_number'        =>  t('Project #'),
    'project_id'            =>  t('Project'),
    'task_id'               =>  t('Task'),
    'regular_time'          =>  t('Reg. Time'),
    'time_1_5'              =>  t('Time @150%'),
    'time_2_0'              =>  t('Time @200%'),
    'evening_hours'         =>  t('Evening Hours'),
    'vacation_time_used'    =>  t('Vacation Time'),
    'floater'               =>  t('Floater'),
    'sick_time'             =>  t('Sick Time'),
    'bereavement'           =>  t('Bereavement'),
    'jury_duty'             =>  t('Jury Duty'),
    'other'                 =>  t('Other'),
    'total_reg_hours'       =>  t('Total HRS.'),
    'comment'               =>  t('Comment'),
    'unpaid_hrs'            =>  t('Unpaid Hrs.')
);
