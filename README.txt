/* $Id:  */

Timesaver Timesheet System.  Feature rich Timesheet application for Drupal.


Installation requirements and instructions:
-------------------------------------------
1. Timesaver requires the Listkeeper module. Please go to the Listkeeper project page to download the module. Save both the Timesaver and Listkeeper modules under your /sites/all/modules directory.

2. You will need to include PHPExcel in the Timesaver module's directory. Please download PHPExcel from this link: http://phpexcel.codeplex.com/releases/view/10719#DownloadId=100806. Save the zip file locally.

3. When you unzip PHPExcel, you will see that there is a "Classes" directory. Opening up the Classes directory reveals a PHPExcel directory and a PHPExcel.php file. Copy PHPExcel.php to the Timesaver module's base directory. From the ZIP file, copy the PHPExcel directory to the Timesaver module's base directory. In the base directory of Timesaver you should see PHPExcel's directory listed with the other Timesaver module directories and you should see PHPExcel.php listed in the same hierarchy with the Timesaver.module, Timesaver.install, Timesaver.info etc. files.

4. Install Timesaver through the modules admin page in Drupal.

5. You will see the Timesheet System link in the left navigation bar. Please see the youtube videos for an overview on how the applicaiton works.



Author:
-------
Randy Kolenko
randy@nextide.ca
